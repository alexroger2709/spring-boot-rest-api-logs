package br.com.logs.spring.boot.rest.api.logs.repository;


import br.com.logs.spring.boot.rest.api.logs.domain.Log;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogResponseRepresentation;
import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Repository
@Slf4j
@Transactional
public class ListarLogRepository {

    public ListarLogResponseRepresentation listarLog(ListarLogRequestRepresentation logRequest) throws LogException {

        String sql;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("logs");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query;
        ListarLogResponseRepresentation retorno = new ListarLogResponseRepresentation();

        //paramêtros para a filtragem
        String whereId;
        String whereData;
        String whereIp;
        String whereRequest;
        String whereStatus;
        String whereUserAgent;

        whereId = obterWhereId(logRequest);
        whereData = obterWhereData(logRequest);
        whereIp = obterWhereIp(logRequest);
        whereRequest = obterWhereRequest(logRequest);
        whereStatus = obterWhereStatus(logRequest);
        whereUserAgent = obterWhereUserAgent(logRequest);


        sql = ("SELECT");
        sql += ("    log_id, ");
        sql += ("    log_data, ");
        sql += ("    log_ip, ");
        sql += ("    log_request, ");
        sql += ("    log_status, ");
        sql += ("    log_user_agent,");
        sql += ("    total_rec,");
        sql += ("    CASE ");
        sql += ("        WHEN ( ( recs_per_page = 0 ) OR ( total_rec < recs_per_page ) OR ( total_rec = 0) )");
        sql += ("            THEN ( 1 )");
        sql += ("        WHEN ( (total_rec % recs_per_page) <> 0 )");
        sql += ("            THEN ( (total_rec / recs_per_page) + 1 )");
        sql += ("        ELSE");
        sql += ("            (total_rec / recs_per_page)");
        sql += ("    END AS total_pages ");
        sql += ("FROM");
        sql += ("    (");
        sql += ("       SELECT");
        sql += ("           " + logRequest.getPaginaAtual() + " AS actual_page,");
        sql += ("           " + logRequest.getRegistrosPorPagina() + " AS recs_per_page,");
        sql += ("           log_id, ");
        sql += ("           log_data, ");
        sql += ("           log_ip, ");
        sql += ("           log_request, ");
        sql += ("           log_status, ");
        sql += ("           log_user_agent,");
        sql += ("           COUNT(*) OVER() AS total_rec,");
        sql += ("           ROW_NUMBER () OVER (ORDER BY log_id) AS rec_id");
        sql += ("       FROM");
        sql += ("       logs");


        //--incluir as cláusulas where
        //sql += ("       WHERE log_request LIKE '%chachacha%'\n");

        sql += (whereId + whereData + whereIp + whereRequest + whereStatus + whereUserAgent);

        sql += ("    ) logsLazy ");
        sql += ("WHERE ");
        sql += ("rec_id BETWEEN (((actual_page*recs_per_page)-recs_per_page)+1) AND (actual_page*recs_per_page);");

        try{
            query = entityManager.createNativeQuery(sql,Log.class);
            retorno.setLogs(query.getResultList());
        }catch(Exception ex){
            throw new LogException(ex.getMessage(),ex);
        }

        entityManager.close();
        entityManagerFactory.close();
        return retorno;
    }


    private boolean verificarFiltro(ListarLogRequestRepresentation logRequest, String campo){
        boolean retorno = false;

        //valida se qualquer paramêtro de filtragem está preenchido
        //mas o nome do paramêtro deve ser diferente do nome de entrada
        boolean id = ( (logRequest.getId() > 0) && (!campo.equals("id")) );
        boolean data = ( (!logRequest.getDataInicio().isEmpty()) || (!logRequest.getDataFim().isEmpty()) ) && (!campo.equals("data"));
        boolean ip = (!logRequest.getIp().isEmpty()) && (!campo.equals("ip"));
        boolean request = (!logRequest.getRequest().isEmpty()) && (!campo.equals("request"));
        boolean status = (logRequest.getStatus() > 0) && (!campo.equals("status"));
        boolean userAgent = (!logRequest.getUserAgent().isEmpty()) && (!campo.equals("userAgent"));

        if(id || data ||  ip || request || status || userAgent){
            retorno = true;
        }

        return retorno;
    }

    private String obterWhereId(ListarLogRequestRepresentation logRequest){
        String retorno = "";

        if(logRequest.getId()>0){
            retorno = " WHERE log_id = " + logRequest.getId() + " ";
        }

        return retorno;
    }

    private String obterWhereData(ListarLogRequestRepresentation logRequest){
        String retorno = "";
        boolean filtro = false;

        filtro = verificarFiltro(logRequest, "data");


        if( (logRequest.getDataInicio() != null) && (!logRequest.getDataInicio().isEmpty()) ){
            if ( filtro ){
                if(logRequest.getDataFim() != null){
                    retorno += " AND log_data\\:\\:DATE BETWEEN '" + logRequest.getDataInicio() + "' AND '" + logRequest.getDataFim() + "' ";
                }else{
                    retorno += " AND log_data\\:\\:DATE = '" + logRequest.getDataInicio() + "' ";
                }
            }else{
                if(logRequest.getDataFim() != null){
                    retorno += " WHERE log_data\\:\\:DATE BETWEEN '" + logRequest.getDataInicio() + "' AND '" + logRequest.getDataFim() + "' ";
                }else{
                    retorno += " WHERE log_data\\:\\:DATE = '" + logRequest.getDataInicio() + "' ";
                }
            }
        }


        return retorno;
    }

    private String obterWhereIp(ListarLogRequestRepresentation logRequest){
        String retorno = "";
        boolean filtro = false;

        filtro = verificarFiltro(logRequest, "ip");


        if( (logRequest.getIp() != null) && (!logRequest.getIp().isEmpty()) ){
            if ( filtro ){
                retorno += " AND log_ip LIKE '%" + logRequest.getIp() + "%' ";
            }else{
                retorno += " WHERE log_ip LIKE '%" + logRequest.getIp() + "%' ";
            }
        }


        return retorno;
    }

    private String obterWhereRequest(ListarLogRequestRepresentation logRequest){
        String retorno = "";
        boolean filtro = false;

        filtro = verificarFiltro(logRequest, "request");

        if( (logRequest.getRequest() != null) && (!logRequest.getRequest().isEmpty()) ){
            if ( filtro ){
                retorno += " AND UPPER(log_request) LIKE '%" + logRequest.getRequest().toUpperCase() + "%' ";
            }else{
                retorno += " WHERE UPPER(log_request) LIKE '%" + logRequest.getRequest().toUpperCase() + "%' ";
            }
        }

        return retorno;
    }

    private String obterWhereStatus(ListarLogRequestRepresentation logRequest){
        String retorno = "";
        boolean filtro = false;

        filtro = verificarFiltro(logRequest, "status");


        if( (logRequest.getStatus() > 0) ){
            if ( filtro ){
                retorno += " AND log_status = " + logRequest.getStatus() + " ";
            }else{
                retorno += " WHERE log_status = " + logRequest.getStatus() + " ";
            }
        }

        return retorno;
    }

    private String obterWhereUserAgent(ListarLogRequestRepresentation logRequest){
        String retorno = "";
        boolean filtro = false;

        filtro = verificarFiltro(logRequest, "userAgent");


        if( (logRequest.getUserAgent() != null) && (!logRequest.getUserAgent().isEmpty()) ){
            if ( filtro ){
                retorno += " AND UPPER(log_user_agent) LIKE '%" + logRequest.getUserAgent().toUpperCase() + "%' ";
            }else{
                retorno += " WHERE UPPER(log_user_agent) LIKE '%" + logRequest.getUserAgent().toUpperCase() + "%' ";
            }
        }

        return retorno;
    }

}
