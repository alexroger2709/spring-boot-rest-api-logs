package br.com.logs.spring.boot.rest.api.logs.representation;

import java.util.List;

public class LogBatchRequestRepresentation {

    //membros
    private List<LogRepresentation> listaLogs;


    //propriedades
    public List<LogRepresentation> getListaLogs() {
        return listaLogs;
    }

    public void setListaLogs(List<LogRepresentation> listaLogs) {
        this.listaLogs = listaLogs;
    }
}
