package br.com.logs.spring.boot.rest.api.logs.representation;

public class ListarLogRequestRepresentation {

    //membros
    private Integer paginaAtual;
    private Integer registrosPorPagina;
    private Integer id;
    private String dataInicio;
    private String dataFim;
    private String ip;
    private String request;
    private Integer status;
    private String userAgent;


    public ListarLogRequestRepresentation(){
        this.paginaAtual = 1;
        this.registrosPorPagina = 10;
        this.id = null;
        this.dataInicio = null;
        this.dataFim = null;
        this.ip = null;
        this.request = null;
        this.status = null;
        this.userAgent = null;
    }

    //propriedades
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(Integer paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    public Integer getRegistrosPorPagina() {
        return registrosPorPagina;
    }

    public void setRegistrosPorPagina(Integer registrosPorPagina) {
        this.registrosPorPagina = registrosPorPagina;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

}
