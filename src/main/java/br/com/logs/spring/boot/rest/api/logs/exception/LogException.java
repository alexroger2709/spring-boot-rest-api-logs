package br.com.logs.spring.boot.rest.api.logs.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class LogException extends Exception {

    public LogException(String message) {
        super(message);
    }

    public LogException(String message, Throwable nested) {
        super(message, nested);
    }
}
