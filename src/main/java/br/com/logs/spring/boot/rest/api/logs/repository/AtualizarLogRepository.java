package br.com.logs.spring.boot.rest.api.logs.repository;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.AtualizarLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Repository
@Slf4j
@Transactional
public class AtualizarLogRepository {

    public OperacaoResponseRepresentation atualizarLog(AtualizarLogRequestRepresentation logAtualizar) throws LogException {

        String sql;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("logs");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Query query;
        OperacaoResponseRepresentation retorno = new OperacaoResponseRepresentation();

        sql = "UPDATE logs SET \n" +
                "log_data = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS:MS')\\:\\:timestamp without time zone,\n" +
                "log_ip = ?, \n" +
                "log_request = ?, \n" +
                "log_status = ?, \n" +
                "log_user_agent = ?\n" +
                "WHERE log_id = ?;\n";

        try{

            transaction.begin();

            query = entityManager.createNativeQuery(sql)
                    .setParameter(1, logAtualizar.getData())
                    .setParameter(2, logAtualizar.getIp())
                    .setParameter(3, logAtualizar.getRequest())
                    .setParameter(4, logAtualizar.getStatus())
                    .setParameter(5, logAtualizar.getUserAgent())
                    .setParameter(6, logAtualizar.getId());

            query.executeUpdate();
            transaction.commit();
        }catch(Exception ex){
            retorno.setStatusOperacao(1);
            retorno.setMensagemOperacao("Erro ao atualizar o log: " + ex.getMessage());
            throw new LogException(ex.getMessage(),ex);
        }

        entityManager.close();
        entityManagerFactory.close();
        return retorno;
    }


}
