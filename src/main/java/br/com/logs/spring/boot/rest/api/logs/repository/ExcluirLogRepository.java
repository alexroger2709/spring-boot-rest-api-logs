package br.com.logs.spring.boot.rest.api.logs.repository;


import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.ExcluirLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Repository
@Slf4j
@Transactional
public class ExcluirLogRepository {

    public OperacaoResponseRepresentation excluirLog(ExcluirLogRequestRepresentation logExcluir) throws LogException {

        String sql;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("logs");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Query query;
        OperacaoResponseRepresentation retorno = new OperacaoResponseRepresentation();

        sql = "DELETE FROM logs \n" +
                "WHERE log_id = ?;\n";

        try{

            transaction.begin();

            query = entityManager.createNativeQuery(sql)
                    .setParameter(1, logExcluir.getId());

            query.executeUpdate();
            transaction.commit();
        }catch(Exception ex){
            retorno.setStatusOperacao(1);
            retorno.setMensagemOperacao("Erro ao excluir o log: " + ex.getMessage());
            throw new LogException(ex.getMessage(),ex);
        }

        entityManager.close();
        entityManagerFactory.close();
        return retorno;
    }

}
