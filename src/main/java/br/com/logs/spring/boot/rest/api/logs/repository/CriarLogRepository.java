package br.com.logs.spring.boot.rest.api.logs.repository;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Repository
@Slf4j
@Transactional
public class CriarLogRepository{


    public OperacaoResponseRepresentation criarLog(LogRequestRepresentation logRequest) throws LogException {

        String sql;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("logs");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Query query;
        OperacaoResponseRepresentation retorno = new OperacaoResponseRepresentation();

        sql = "INSERT INTO logs(log_id, log_data, log_ip, log_request, log_status, log_user_agent) VALUES"
                + "("
                + "(SELECT COALESCE(MAX(log_id),0)+1 FROM logs),"
                + "TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS:MS')\\:\\:timestamp without time zone,?,?,?,?);";

        try{

            transaction.begin();

                query = entityManager.createNativeQuery(sql)
                        .setParameter(1, logRequest.getData())
                        .setParameter(2, logRequest.getIp())
                        .setParameter(3, logRequest.getRequest())
                        .setParameter(4, logRequest.getStatus())
                        .setParameter(5, logRequest.getUserAgent());

                query.executeUpdate();
            transaction.commit();
        }catch(Exception ex){
            retorno.setStatusOperacao(1);
            retorno.setMensagemOperacao("Erro ao criar o log: " + ex.getMessage());
            throw new LogException(ex.getMessage(),ex);
        }

        entityManager.close();
        entityManagerFactory.close();
        return retorno;
    }

}
