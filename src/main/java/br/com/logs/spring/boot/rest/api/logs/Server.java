package br.com.logs.spring.boot.rest.api.logs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;


@Configuration
@SpringBootApplication(scanBasePackages = "br.com.logs.spring.boot.rest.api.logs")
public class Server {

	public static void main(String[] args) {
		SpringApplication.run(Server.class, args);
	}
}
