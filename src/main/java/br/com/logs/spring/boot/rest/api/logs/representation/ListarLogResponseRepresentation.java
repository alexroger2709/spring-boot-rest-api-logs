package br.com.logs.spring.boot.rest.api.logs.representation;

import br.com.logs.spring.boot.rest.api.logs.domain.Log;

import java.util.List;

public class ListarLogResponseRepresentation {

    //membros
    private List<Log> logs;


    //propriedades
    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }
}
