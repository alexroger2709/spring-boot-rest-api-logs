package br.com.logs.spring.boot.rest.api.logs.restservice;

import br.com.logs.spring.boot.rest.api.logs.business.ExcluirLogService;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.ExcluirLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;


@Api(basePath = "/logsApp", value = "API para Exclusão de Logs", description = "API para Exclusão de Logs")
@RestController
@RequestMapping(path = "/logsApp")
public class ExcluirLogApi {

    private ExcluirLogService servico;

    @Autowired
    public ExcluirLogApi(ExcluirLogService servico){
        this.servico = servico;
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Processo realizado com sucesso"),
            @ApiResponse(code = 500, message = "Erro interno")})
    @DeleteMapping(value = "/excluirLog", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> excluirLog(@RequestBody ExcluirLogRequestRepresentation logRequest) throws LogException {

        ResponseEntity<OperacaoResponseRepresentation> retorno;

        try {
            retorno = new ResponseEntity<>(servico.excluirLog(logRequest), HttpStatus.OK);
        } catch (Exception ex) {
            throw new LogException("Erro na exclusão do log: " + ex.getMessage());
        }

        return retorno;
    }


}
