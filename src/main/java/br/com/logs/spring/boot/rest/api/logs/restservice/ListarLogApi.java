package br.com.logs.spring.boot.rest.api.logs.restservice;


import br.com.logs.spring.boot.rest.api.logs.business.ListarLogService;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

@CrossOrigin
@Api(basePath = "/logsApp", value = "API para Listagem de Logs", description = "API para Listagem de Logs")
@RestController
@RequestMapping(path = "/logsApp")
public class ListarLogApi {

    private ListarLogService servico;

    @Autowired
    public ListarLogApi(ListarLogService servico){
        this.servico = servico;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Processo realizado com sucesso"),
            @ApiResponse(code = 500, message = "Erro interno")})
    @PostMapping(value = "/listarLog", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> listarLog(@RequestBody ListarLogRequestRepresentation logRequest) throws LogException {

        ResponseEntity<ListarLogResponseRepresentation> retorno;

        try {
            retorno = new ResponseEntity<>(servico.listarLog(logRequest), HttpStatus.OK);
        } catch (Exception ex) {
            throw new LogException("Erro na listagem do log: " + ex.getMessage());
        }

        return retorno;
    }

}
