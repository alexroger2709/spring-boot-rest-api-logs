package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.repository.AtualizarLogRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.AtualizarLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AtualizarLogService {

    private AtualizarLogRepository repository;

    @Autowired
    public AtualizarLogService(AtualizarLogRepository repository){
        this.repository = repository;
    }

    public OperacaoResponseRepresentation atualizarLog(AtualizarLogRequestRepresentation logRequest) throws LogException
    {
        OperacaoResponseRepresentation retorno;

        try{
            retorno = repository.atualizarLog(logRequest);
        }catch(Exception ex){
            throw new LogException(ex.getMessage(), ex);
        }

        return retorno;
    }


}
