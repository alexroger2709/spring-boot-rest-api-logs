package br.com.logs.spring.boot.rest.api.logs.restservice;

import br.com.logs.spring.boot.rest.api.logs.business.CriarLogLoteService;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin
@Api(basePath = "/logsApp", value = "API Criação de Logs em Lote", description = "API Criação de Logs em Lote")
@RestController
@RequestMapping(path = "/logsApp")
public class CriarLogLoteApi {

    private CriarLogLoteService servico;

    @Autowired
    public CriarLogLoteApi(CriarLogLoteService servico){
        this.servico = servico;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Processo realizado com sucesso"),
            @ApiResponse(code = 500, message = "Erro interno")})
    @PostMapping(value = "/criarLogLote", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> criarLogLote(@RequestParam("file") MultipartFile file) throws LogException {

        ResponseEntity<OperacaoResponseRepresentation> retorno;

        try {
            retorno = new ResponseEntity<>(servico.criarLogLote(file), HttpStatus.OK);
        } catch (Exception ex) {
            throw new LogException("Erro na criação do log: " + ex.getMessage());
        }


        return retorno;
    }


}
