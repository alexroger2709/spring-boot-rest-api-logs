package br.com.logs.spring.boot.rest.api.logs.repository;


import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.apache.logging.log4j.Level;
import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


@Repository
@Slf4j
@Transactional
public class CriarLogLoteRepository {

    private Logger logger = LogManager.getLogger(CriarLogLoteRepository.class);

    public OperacaoResponseRepresentation criarLogLote(List<LogRequestRepresentation> lote) throws Exception {

        String sql;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("logs");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Query query;
        OperacaoResponseRepresentation retorno = new OperacaoResponseRepresentation();
        int contadorLote=0;

        sql = "INSERT INTO logs(log_id, log_data, log_ip, log_request, log_status, log_user_agent) VALUES"
                + "("
                + "(SELECT COALESCE(MAX(log_id),0)+1 FROM logs),"
                + "TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS:MS')\\:\\:timestamp without time zone,?,?,?,?);";


            for (LogRequestRepresentation logRequest : lote){
                try{
                    if(contadorLote==0){
                        transaction.begin();
                    }

                    contadorLote++;
                    query = entityManager.createNativeQuery(sql)
                            .setParameter(1, logRequest.getData())
                            .setParameter(2, logRequest.getIp())
                            .setParameter(3, logRequest.getRequest().replace("\"",""))
                            .setParameter(4, logRequest.getStatus())
                            .setParameter(5, logRequest.getUserAgent().replace("\"",""));

                    query.executeUpdate();

                    //a cada 1000 registros, commita a operação
                    if (contadorLote % 5000 == 0) {
                        transaction.commit();
                        contadorLote = 0;
                    }
                }catch(Exception ex){
                    //logar o erro no servidor de aplicação
                    //e continuar o processo
                    logger.error("Erro ao inserir o log no banco de dados: " + logRequest.toString(), ex);
                }
            }

            //se ficarem registros em aberto
            //executa seu commit
            if (contadorLote>0){
                transaction.commit();
            }

        entityManager.close();
        entityManagerFactory.close();
        return retorno;
    }

}
