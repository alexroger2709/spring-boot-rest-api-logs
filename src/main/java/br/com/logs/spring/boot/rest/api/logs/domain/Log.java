package br.com.logs.spring.boot.rest.api.logs.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Log {


    //membros
    @Id
    @Column(name="log_id")
    private Integer id;

    @Column(name="log_data")
    private String data;

    @Column(name="log_ip")
    private String ip;

    @Column(name="log_request")
    private String request;

    @Column(name="log_status")
    private Integer status;

    @Column(name="log_user_agent")
    private String userAgent;

    @Column(name="total_rec")
    private Integer totalRegistros;


    @Column(name="total_pages")
    private Integer totalPaginas;



    //propriedades
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Integer getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(Integer totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }
}
