package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.repository.ExcluirLogRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.ExcluirLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExcluirLogService {

    private ExcluirLogRepository repository;

    @Autowired
    public ExcluirLogService(ExcluirLogRepository repository){
        this.repository = repository;
    }

    public OperacaoResponseRepresentation excluirLog(ExcluirLogRequestRepresentation logRequest) throws LogException
    {
        OperacaoResponseRepresentation retorno;

        try{
            retorno = repository.excluirLog(logRequest);
        }catch(Exception ex){
            throw new LogException(ex.getMessage(), ex);
        }

        return retorno;
    }

}
