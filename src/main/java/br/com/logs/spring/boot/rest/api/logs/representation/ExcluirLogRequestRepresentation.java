package br.com.logs.spring.boot.rest.api.logs.representation;

public class ExcluirLogRequestRepresentation {

    //membros
    private Integer id;


    //propriedades
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
