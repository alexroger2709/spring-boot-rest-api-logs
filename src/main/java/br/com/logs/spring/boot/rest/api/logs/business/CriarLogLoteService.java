package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.repository.CriarLogLoteRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CriarLogLoteService {

    private Logger logger = LogManager.getLogger(CriarLogLoteService.class);

    private CriarLogLoteRepository repository;

    @Autowired
    public CriarLogLoteService(CriarLogLoteRepository repository){
        this.repository = repository;
    }


    public OperacaoResponseRepresentation criarLogLote(MultipartFile file) throws Exception{

        OperacaoResponseRepresentation retorno = new OperacaoResponseRepresentation();
        String linha;
        String[] log;
        BufferedReader reader;
        List<LogRequestRepresentation> lote = new ArrayList<>();

        if (!file.isEmpty()) {
            reader = obterReader(file);
            linha = reader.readLine();

            while (linha != null) {
                try {
                    log = linha.split("\\|");

                    //novo objeto do lote
                    LogRequestRepresentation request = new LogRequestRepresentation();
                    request.setData(log[0]);
                    request.setIp(log[1]);
                    request.setRequest(log[2]);
                    request.setStatus(Integer.parseInt(log[3]));
                    request.setUserAgent(log[4]);
                    lote.add(request);

                }catch (Exception ex) {
                    //logar o erro no servidor de aplicação
                    //e continuar o processo
                    logger.error("Erro ao ler o lote na linha: " + linha, ex);
                }
                linha = reader.readLine();
            }
            reader.close();


            if (!lote.isEmpty()){
                repository.criarLogLote(lote);
            }
        }

        return retorno;
    }

    public BufferedReader obterReader(MultipartFile file) throws Exception{
        BufferedReader reader;
        InputStream stream;

        stream = file.getInputStream();
        reader = new BufferedReader(new InputStreamReader(stream));

        return reader;
    }
}
