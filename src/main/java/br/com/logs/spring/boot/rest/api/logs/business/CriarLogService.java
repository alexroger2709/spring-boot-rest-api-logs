package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.repository.CriarLogRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CriarLogService {
    private CriarLogRepository repository;

    @Autowired
    public CriarLogService(CriarLogRepository repository){
        this.repository = repository;
    }

    public OperacaoResponseRepresentation criarLog(LogRequestRepresentation logRequest) throws LogException
    {
        OperacaoResponseRepresentation retorno;

        try{
            retorno = repository.criarLog(logRequest);
        }catch(Exception ex){
            throw new LogException(ex.getMessage(), ex);
        }

        return retorno;
    }


}
