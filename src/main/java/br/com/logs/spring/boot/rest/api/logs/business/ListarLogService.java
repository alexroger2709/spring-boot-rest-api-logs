package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.repository.ListarLogRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.ListarLogResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ListarLogService {

    private ListarLogRepository repository;

    @Autowired
    public ListarLogService(ListarLogRepository repository){
        this.repository = repository;
    }

    public ListarLogResponseRepresentation listarLog(ListarLogRequestRepresentation logRequest) throws LogException
    {
        ListarLogResponseRepresentation retorno = new ListarLogResponseRepresentation();
        try{
            retorno = repository.listarLog(logRequest);
        }catch(Exception ex){
            throw new LogException(ex.getMessage(), ex);
        }

        return retorno;
    }

}
