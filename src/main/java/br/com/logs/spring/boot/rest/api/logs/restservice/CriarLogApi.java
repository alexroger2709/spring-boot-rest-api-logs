package br.com.logs.spring.boot.rest.api.logs.restservice;

import br.com.logs.spring.boot.rest.api.logs.business.CriarLogService;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;


@Api(basePath = "/logsApp", value = "API Criação de Logs", description = "API Criação de Logs")
@RestController
@RequestMapping(path = "/logsApp")
public class CriarLogApi {

    private CriarLogService servico;

    @Autowired
    public CriarLogApi(CriarLogService servico){
        this.servico = servico;
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Processo realizado com sucesso"),
            @ApiResponse(code = 500, message = "Erro interno")})
    @PostMapping(value = "/criarLog", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> criarLog(@RequestBody LogRequestRepresentation logRequest) throws LogException {

        ResponseEntity<OperacaoResponseRepresentation> retorno;

        try {
            retorno = new ResponseEntity<>(servico.criarLog(logRequest), HttpStatus.OK);
        } catch (Exception ex) {
            throw new LogException("Erro na criação do log: " + ex.getMessage());
        }

        return retorno;
    }
}
