package br.com.logs.spring.boot.rest.api.logs.representation;

import br.com.logs.spring.boot.rest.api.logs.domain.Log;

/**
 * classe utilizada para o retorno da inclusão ou modificação do log na base
 */
public class OperacaoResponseRepresentation {

    //membros
    //0 - sucesso ou 1 - erro
    private Integer statusOperacao;

    //mensagem da operação sucesso ou erro com a exceção ocorrida
    private String mensagemOperacao;



    public OperacaoResponseRepresentation(){
        this.statusOperacao = 0;
        this.mensagemOperacao = "Operação executada com sucesso!";
    }

    //propriedades
    public Integer getStatusOperacao() {
        return statusOperacao;
    }

    public void setStatusOperacao(Integer statusOperacao) {
        this.statusOperacao = statusOperacao;
    }

    public String getMensagemOperacao() {
        return mensagemOperacao;
    }

    public void setMensagemOperacao(String mensagemOperacao) {
        this.mensagemOperacao = mensagemOperacao;
    }
}
