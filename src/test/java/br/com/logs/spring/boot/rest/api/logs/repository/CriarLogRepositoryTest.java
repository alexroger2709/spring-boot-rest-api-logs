package br.com.logs.spring.boot.rest.api.logs.repository;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.*;


@RunWith(MockitoJUnitRunner.Silent.class)
public class CriarLogRepositoryTest {

    private CriarLogRepository repository = new CriarLogRepository();
    private OperacaoResponseRepresentation logResponse = new OperacaoResponseRepresentation();
    private LogRequestRepresentation logRequest = new LogRequestRepresentation();

    private EntityManagerFactory entityManagerFactory = Mockito.mock(EntityManagerFactory.class);
    private EntityManager entityManager = Mockito.mock(EntityManager.class);
    private EntityTransaction transaction = Mockito.mock(EntityTransaction.class);
    private Query query = Mockito.mock(Query.class);

    @Test
    public void criarLogTest() throws Exception{

        logRequest.setData("2019-01-01 00:00:54.583");
        logRequest.setIp("192.168.234.82");
        logRequest.setRequest("GET / HTTP/1.1");
        logRequest.setStatus(200);
        logRequest.setUserAgent("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0");


        Mockito.when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);
        Mockito.when(entityManager.getTransaction()).thenReturn(transaction);

        Mockito.when(entityManager.createNativeQuery("SELECT 1")).thenReturn(query);
        Mockito.when(query.executeUpdate()).thenReturn(0);

        Assert.assertNotNull(repository.criarLog(logRequest));
    }

    @Test(expected = LogException.class)
    public void criarLogTestException() throws Exception{
        Mockito.when(repository.criarLog(logRequest)).thenThrow(LogException.class);
        logResponse = repository.criarLog(logRequest);
    }

}
