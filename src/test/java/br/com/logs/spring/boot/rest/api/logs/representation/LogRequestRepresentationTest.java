package br.com.logs.spring.boot.rest.api.logs.representation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LogRequestRepresentationTest {

    @Test
    public void teste() {
        LogRequestRepresentation log = new LogRequestRepresentation();
        log.setData("");
        log.setIp("");
        log.setStatus(200);
        log.setUserAgent("");
        log.setRequest("");

        Assert.assertNotNull(log.getData());
        Assert.assertNotNull(log.getIp());
        Assert.assertNotNull(log.getStatus());
        Assert.assertNotNull(log.getUserAgent());
        Assert.assertNotNull(log.getRequest());
    }

}