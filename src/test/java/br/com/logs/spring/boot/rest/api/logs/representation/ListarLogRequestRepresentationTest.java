package br.com.logs.spring.boot.rest.api.logs.representation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ListarLogRequestRepresentationTest {

    @Test
    public void teste() {
        ListarLogRequestRepresentation listarLogs = new ListarLogRequestRepresentation();
        listarLogs.setPaginaAtual(1);
        listarLogs.setRegistrosPorPagina(10);
        listarLogs.setDataInicio("");
        listarLogs.setDataFim("");
        listarLogs.setIp("");
        listarLogs.setRequest("");
        listarLogs.setStatus(200);
        listarLogs.setUserAgent("");

        Assert.assertNotNull(listarLogs.getPaginaAtual());
        Assert.assertNotNull(listarLogs.getRegistrosPorPagina());
        Assert.assertNotNull(listarLogs.getDataInicio());
        Assert.assertNotNull(listarLogs.getDataFim());
        Assert.assertNotNull(listarLogs.getIp());
        Assert.assertNotNull(listarLogs.getRequest());
        Assert.assertNotNull(listarLogs.getStatus());
        Assert.assertNotNull(listarLogs.getUserAgent());

    }


}