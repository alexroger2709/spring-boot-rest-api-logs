package br.com.logs.spring.boot.rest.api.logs.business;

import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.repository.CriarLogRepository;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import br.com.logs.spring.boot.rest.api.logs.representation.OperacaoResponseRepresentation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;



@RunWith(MockitoJUnitRunner.Silent.class)
public class CriarLogServiceTest {

    private OperacaoResponseRepresentation logResponse = new OperacaoResponseRepresentation();
    private LogRequestRepresentation logRequest = new LogRequestRepresentation();
    private CriarLogRepository repository = Mockito.mock(CriarLogRepository.class);
    private CriarLogService logService = new CriarLogService(repository);


    @Test
    public void testarServico() throws Exception{
        Mockito.when(repository.criarLog(ArgumentMatchers.any(LogRequestRepresentation.class))).thenReturn(logResponse);

        logResponse = logService.criarLog(logRequest);
        Assert.assertNotNull(logResponse);
    }


    @Test(expected = LogException.class)
    public void testarException() throws Exception{
        Mockito.when(logService.criarLog(logRequest)).thenThrow(LogException.class);
        logResponse = logService.criarLog(logRequest);
    }

}