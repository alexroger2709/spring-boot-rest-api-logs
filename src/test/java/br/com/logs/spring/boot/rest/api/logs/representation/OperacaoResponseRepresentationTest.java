package br.com.logs.spring.boot.rest.api.logs.representation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OperacaoResponseRepresentationTest {

    @Test
    public void teste() {
        OperacaoResponseRepresentation logResponse = new OperacaoResponseRepresentation();
        logResponse.setStatusOperacao(0);
        logResponse.setMensagemOperacao("");

        Assert.assertNotNull(logResponse.getStatusOperacao());
        Assert.assertNotNull(logResponse.getMensagemOperacao());
    }

}