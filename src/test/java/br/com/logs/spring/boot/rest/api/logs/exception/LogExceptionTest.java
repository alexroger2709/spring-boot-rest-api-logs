package br.com.logs.spring.boot.rest.api.logs.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LogExceptionTest {

    @Test(expected= LogException.class)
    public void teste() throws LogException {
        throw new LogException("Teste da classe customizada de exceção");
    }
}