package br.com.logs.spring.boot.rest.api.logs.restservice;

import br.com.logs.spring.boot.rest.api.logs.business.CriarLogService;
import br.com.logs.spring.boot.rest.api.logs.exception.LogException;
import br.com.logs.spring.boot.rest.api.logs.representation.LogRequestRepresentation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;


@RunWith(MockitoJUnitRunner.Silent.class)
public class CriarLogApiTest {

    private CriarLogService service = Mockito.mock(CriarLogService.class);
    private LogRequestRepresentation request = new LogRequestRepresentation();

    @Test
    public void testar() throws Exception{
        CriarLogApi api = new CriarLogApi(service);

        ResponseEntity<?> response = api.criarLog(request);
        Assert.assertNotNull(api);
        Assert.assertNotNull(response);
    }


    @Test(expected = LogException.class)
    public void testarException() throws Exception{
        CriarLogApi api = new CriarLogApi(service);

        Mockito.when(api.criarLog(request)).thenThrow(LogException.class);
        ResponseEntity<?> response = api.criarLog(request);
    }
}