package br.com.logs.spring.boot.rest.api.logs.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LogTest {

    @Test
    public void teste() {
        Log log = new Log();
        log.setId(1);
        log.setData("");
        log.setIp("");
        log.setStatus(200);
        log.setUserAgent("");
        log.setRequest("");

        Assert.assertNotNull(log.getId());
        Assert.assertNotNull(log.getData());
        Assert.assertNotNull(log.getIp());
        Assert.assertNotNull(log.getStatus());
        Assert.assertNotNull(log.getUserAgent());
        Assert.assertNotNull(log.getRequest());
    }

}